jQuery(document).ready(function() {

    // Map Initial Location
    var initLatitude = -19.875397;
    var initLongitude = -43.935245;
    
    var mapa = new GMaps({
        div: '#map',
        scrollwheel: false,
        //markers: mapMarkers,
        lat: initLatitude,
        lng: initLongitude
    });
    
    mapa.addMarker({
        lat: initLatitude,
        lng: initLongitude,
        title: 'OfficeApp',
        infoWindow: {
            content: 'Escritório Central da OfficeApp'
        }
    });

});