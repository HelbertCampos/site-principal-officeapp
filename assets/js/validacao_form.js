jQuery(document).ready(function() {
    
    // Valida formulário de contato
	$("#FormularioContato").validate({
        rules: {
            ContatoNome: {
                required: true,
                minlength: 3,
                maxlength: 70,
            },
            ContatoEmail: {
                required: true,
                email: true,
            },
            ContatoAssunto: {
                required: true,
                minlength: 3,
                maxlength: 70,
            },
            ContatoMensagem: {
                required: true
            },
        },
        messages: {
            ContatoNome: {
                required: 'Por favor, informe seu nome para que possamos entrar em contato com você.',
		        minlength: jQuery.validator.format("O nome deve ter no mínimo {0} caracteres."),
		        maxlength: jQuery.validator.format("Por favor, o nome não pode ter mais que {0} caracteres."),
            },
            ContatoEmail: {
                required: 'Por favor, informe seu email para que possamos entrar em contato com você.',
                email: 'O email informado não é válido.',
            },
            ContatoAssunto: {
                required: 'Por favor, informe um assunto para sua mensagem.',
		        minlength: jQuery.validator.format("O assunto deve ter no mínimo {0} caracteres."),
		        maxlength: jQuery.validator.format("Por favor, o assunto não pode ter mais que {0} caracteres."),
            },
            ContatoMensagem: {
                required: 'Não seja tímido, deixe-nos uma mensagem.',				
            },
        },
        submitHandler: function(frm) {
            
            $('#FormularioContato .alert').addClass('alert-info').html('<i class="fa fa-refresh fa-spin"></i> Aguarde, enviando mensagem ...');

            var enviaMsg = true;
            if (enviaMsg) {

                $('#FormularioContato .alert').removeClass('alert-info').addClass('alert-success').html('<i class="fa fa-check-circle"></i> Mensagem enviada com sucesso!');
                
                var msg = 'Olá '+$('#ContatoNome').val()+' agradecemos a sua mensagem!</br>Em breve, alguém da equipe OfficeApp entrará em contato com você! :)'
                $('#ModalPrincipal .Titulo').html('Mensagem recebida com sucesso!');
                $('#ModalPrincipal .Mensagem').html(msg);
                $('#ModalPrincipal').modal();

                $('#ModalPrincipal .ModalOk').click(function() {

                   window.location.reload(); 

                });
                
            } else {
                $('#FormularioContato .alert').removeClass('alert-info').addClass('alert-danger').html('<i class="fa fa-exclamation-circle"></i> Desculpe, ocorreu um erro ao tentar enviar a mensagem. :( </br>Por favor, envie-nos um email com sua mensagem para <a href="mailto:officeapp@officeapp.com.br">officeapp@officeapp.com.br</a>');            
            };
            
        }
    });
    
    // Click do registro
    $('button.FazerRegistro').click(function() {
        $('#FormularioRegistro').submit()
    });
    
    // Valida email de inscrição
	$("#FormularioRegistro").validate({
        rules: {
            EmailRegistro: {
                required: true,
                email: true,
            },
        },
        messages: {
            EmailRegistro: {
                required: 'Por favor, informe seu email.',
                email: 'O email informado não é válido.',
            },
        },
        submitHandler: function(frm) {
            
            console.log('etste');
            
        }
    });


});